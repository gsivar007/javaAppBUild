pipeline {
    agent none
    parameters{
        booleanParam(name: 'CLEANBUILD', defaultValue: true, description:'Enable clean build?')
    }

    stages {
        stage('checkout') {
            agent {label 'gcp'}
            steps {
                git credentialsId: 'GitLabCred', url: 'https://gitlab.com/cmgeorge9802/mavenbuild.git'
            }
        }
        
        stage('webHookCause'){
            when{
                beforeAgent true
                triggeredBy 'GitLabWebHookCause'
            }
            agent {label 'gcp'}
            steps{
                echo "I am only executed when triggered by SCM push"
                script{
                    env.BUILDTYPE = 'CI'
                }
            }
        }
        
        stage('Manual Timed'){
            when{
                beforeAgent true
                anyOf{
                    triggeredBy 'TimerTrigger'
                    triggeredBy cause: 'UserIdCause'
                }
            }
            agent {label 'gcp'}
            steps{
                echo "I am only executed when triggered manually or timed"
                script{
                    env.BUILDTYPE = 'FULL'
                }
            }
        }
        
        stage('validate commit'){
            when{ 
                environment name: 'BUILDTYPE', value: 'CI'
                anyOf{
                    changeset "samplejar/**"
                    changeset "samplewar/**"
                }
            }
            steps{
                script{
                    env.BUILDME = "yes"
                }
            }
        }
        stage('Build'){
        when {
            anyOf{
                environment name: 'BUILDME', value: 'yes'
                environment name: 'BUILDTYPE', value: 'FULL'
            }
        }
        agent {label 'gcp'}
        steps {
        script {
            if (params.CLEANBUILD) {
              cleanstr = "clean"
            } else {
              cleanstr = ""
            }

            echo "Building Jar Component ..."
            dir ("./samplejar") {
               sh "export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64; mvn ${cleanstr} package"
            }

            echo "Building War Component ..."
            dir ("./samplewar") {
              sh "export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64; mvn ${cleanstr} package"
            }
          }
         }
        }
        stage('Code Coverage')
        {
            when {
                anyOf{
                    environment name: 'BUILDME', value: 'yes'
                    environment name: 'BUILDTYPE', value: 'FULL'
                }
            }
            agent {label 'gcp'}
            steps {
                echo "Running Code Coverage ..."
                dir ("./samplejar") {
                    sh "mvn org.jacoco:jacoco-maven-plugin:0.5.5.201112152213:prepare-agent"
                }
            }
        }
        
        stage('Artifactory'){
            when{
                anyOf{
                    environment name: 'BUILDME', value: 'yes'
                    environment name: 'BUILDTYPE', value: 'FULL'
                }
            }
            agent {label 'gcp'}
            steps {
                script{
                /* Define the artifactory server details */
                    def server = Artifactory.server 'milantechfrog'
                    def uploadSpec = """{
                        "files": [{
                        "pattern": "samplewar/target/samplewar.war",
                        "target": "milantech"
                        }]
                    }"""
                    /* Upload the war to Artifactory repo */
                    server.upload(uploadSpec)
                }
            }
        }
        
        
        
    } // end of stage
}
